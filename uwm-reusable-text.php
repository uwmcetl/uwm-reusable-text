<?php
/**
 * @package   UWM Reusable Text
 * @author    Catarino David Delgado <delgadcd@uwm.edu>
 * @license   MIT
 * @link      https://bitbucket.org/uwmcetl/uwm-reusable-text/src/
 * @copyright 2020 University of Wiscosnin-Milwaukee
 *
 * @wordpress-plugin
 * Plugin Name: UWM Reusable Text
 * Plugin URI:  https://bitbucket.org/uwmcetl/uwm-reusable-text/src/
 * Description: Re-use bits of text and HTML
 * Version:     0.1
 * Author:      Catarino David Delgado <delgadcd@uwm.edu>
 * Author URI:  https://uwm.edu/cetl/people/delgado-david/
 * License:     MIT
 * License URI: https://bitbucket.org/uwmcetl/uwm-reusable-text/src/master/LICENSE.txt
 */


// Constants
define("URT_POST_TYPE", "uwm-reusable-text");
define("URT_POST_TYPE_NAME", "Reusable Text Block");
define("URT_TOKEN_MASK", "/\{\{[A-Za-z0-9\-\_\+\=]*\}\}/i");


// If not in WordPress, don't execute code
if ( !defined( 'ABSPATH' ) ) exit;


/**
 * Create the post type on page load
 */
function uwmrt_create_post_type()
{

    // Supported post functions
    $post_type_supports = array(
        'title',
        'editor',
        'author',
        'custom-fields',
        'revisions'
    );

    // Labels
    $post_type_labels = array(
        'name'          =>  URT_POST_TYPE_NAME . 's',
        'singular_name' =>  URT_POST_TYPE_NAME
    );

    // Supported taxonomies
    $post_type_taxonomies = array(
        'post_tag',
        'category'
    );

    // Create the post type
    $post_type_options = array(
        'supports'              =>  $post_type_supports,
        'labels'                =>  $post_type_labels,
        'public'                =>  false,
        'hierarchical'          =>  false,
        'publicly_queryable'    =>  true,
        'show_ui'               =>  true,
        'show_in_nav_menus'     =>  false,
        'show_in_admin_bar'     =>  true,
        'show_in_rest'          =>  false,
        'menu_icon'             =>  'dashicons-media-code',
        'capability_type'       =>  'page',
        'exclude_from_search'   =>  true,
        'taxonomies'            =>  $post_type_taxonomies,        
        'has_archive'           =>  false,
        'rewrite'               =>  false,
        'can_export'            =>  true,		
        'delete_with_user'      =>  false,
        
    );
    register_post_type( URT_POST_TYPE, $post_type_options );

}
add_action( 'init', 'uwmrt_create_post_type' );


/**
 * Render the uwm_reusable_text shortcode by getting the ID of a post, and parse
 * other attributes, fill-in the template, and execute shortcodes
 * 
 * @param array $atts shortcode attributes
 * @return string parsed content
 */
function uwmrt_shortcode_render( $atts )
{
    
    $response = '';

    $tag_defaults = array(
        'id'    =>  null
    );
    $params = shortcode_atts( $tag_defaults, $atts );

    // Extract variables
    $variables = array_diff_key( $atts, $tag_defaults );

    // Get content of the post referred to by ID
    $post_data = get_post( $params[ 'id' ] );
    $post_content = $post_data->post_content;

    // Are there template tokens?
    if ( preg_match( URT_TOKEN_MASK, $post_content ) )
    {

        // Replace tokens with values
        foreach( $variables as $key => $value )
        {
            $post_content = str_replace( '{{' . $key . '}}', $value, $post_content );
        }

    }

    // Process shortcodes
    $response = apply_filters( 'the_content', $post_content );

    return $response;

}
add_shortcode( 'uwm_reusable_text', 'uwmrt_shortcode_render' );