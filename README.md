# UWM Reusable Text #

This plugin creates a post type that allows the user to make reusable text, HTML or shortcode blocks. A simple template
system allows for blocks to be flexible.

You can use this plugin to make a template for your custom post type when used with the [Custom Content Shortcode plugin](https://wordpress.org/plugins/custom-content-shortcode/).

## How to Install ##

* Go to the Downloads page of this repository and save a zip. Then, [upload the plugin to Wordpress](https://wordpress.org/support/article/managing-plugins/#manual-upload-via-wordpress-admin) and activate.

## How to Use ##

### Create a new Reusable Text Block ###

Use the Reusable Text Block menu in the admin bar.

1. Click "Resuable Text Blocks" in the WordPress Admin menu.
2. This is a 
3. Click the "Add New" link.
4. Enter a title, and your content
5. Select any categories or taxonomies as needed
6. Click the Publish or Update button

### Insert the Reusable Block ###

Use the uwm_reusable_text shortcode with the ID of the post to insert the block.

    [uwm_reusable_text id=300]

The content of the block with ID 300 will be output in-place. Note that there is no extra HTML added to the output.

### Using tokens to use the reusable block as a template ###

Text wrapped in double-curley braces ("{{" and "}}") will be parsed and replaced with parameters given in the shortcode. For example,
if you place the following text in a block...

    <h2>{{heading}}</h2>
    <p>Welcome {{name}}!<br>
    We are happy to see you at {{time}} on {{date}}.</p>

... specify the values of the heading, name, time and date tokens in the shortcode.

    [uwm_reusable_text id=300 heading='Good Evening!' name='Jamie' time='7:00pm' date='December 10']

The output to the shortcode will be

    <h2>Good Evening!</h2>
    <p>Welcome Jamie!<br>
    We are happy to see you at 7:00pm on December 10.</p>

Token names may only contain letters, numbers, dashes and underscores.

### Blocks, Tokens and Shortcodes ###

Any shortcodes included in blocks will be processed after token are replaced. Example:

    <p>
    [img src="{{url}}" alt="{{alt_text}}"]
    <strong>{{name}}</strong>
    </p>

For this block, the url, alt_text and name tokens will be replaced. Then, the img shortcode will be processed.

## Support ##

* Email the author (delgadcd@uwm.edu) to report an issue.